import { setToken, getToken, delToken } from '@/utils/storage'
import { login } from '@/api/user/user.js'
// 两根线
// 同步：组件 commit -> mutations -> state
// 异步： 组件 dispatch -> actions -> commit -> mutations -> state
export default {
  // 开启命名空间
  namespaced: true,
  // 放数据的地方
  state: () => ({
    token: getToken() || ''
  }),
  // 相当于组件中计算属性
  getters: {},
  // 这个是唯一修改state中数据地方
  mutations: {
    updateToken (state, payload) {
      state.token = payload
      setToken(payload)
    },
    // 删除本地存储token
    removeToken (state, payload) {
      state.token = payload
    }
  },

  actions: {
    async login (context, payload) {
      const res = await login(payload)
      // console.log(res)
      context.commit('updateToken', res.data.data.token)
    },
    userOut (context) {
      delToken()
      context.commit('removeToken', '')
    }
  }
}
