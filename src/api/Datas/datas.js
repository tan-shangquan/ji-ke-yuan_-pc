import request from '@/utils/request.js'
// 获取所有或筛选文章
export const datas = (params = '') => {
  return request({
    url: '/v1_0/mp/articles',
    params
  })
}
// 获取频道列表
export const CCTVall = () => {
  return request({
    method: 'GET',
    url: '/v1_0/channels'
  })
}
// 删除文章
export const deldata = (id) => {
  return request({
    method: 'DELETE',
    url: `/v1_0/mp/articles/${id}`
  })
}
// 获取文章详情
export const detail = (id) => {
  return request({
    url: `/v1_0/mp/articles/${id}`
  })
}
// 编辑文章
export const compile = data => {
  return request({
    method: 'PUT',
    url: `/v1_0/mp/articles/${data.id}?draft=${data.draft}`,
    data
  })
}
// 发表文章
export const news = (data) => {
  return request({
    method: 'POST',
    url: `/v1_0/mp/articles?draft=${data.draft}`,
    data
  })
}
