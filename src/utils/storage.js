const KEY = 'jikeyuan_pc_token'
export const setToken = (newToken) => {
  localStorage.setItem(KEY, newToken)
}
export const getToken = () => {
  return localStorage.getItem(KEY)
}
export const delToken = () => {
  localStorage.removeItem(KEY)
}
