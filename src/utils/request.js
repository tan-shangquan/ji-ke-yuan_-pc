import axios from 'axios'
import store from '@/store' // 引入store
const service = axios.create({
  baseURL: 'http://toutiao.itheima.net',
  tiemout: 8000
})
// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  config.headers.Authorization = `Bearer ${store.state.user.token}`
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})
// service.interceptors.response.use(function (response) {
//   // 2xx 范围内的状态码都会触发该函数。
//   // 对响应数据做点什么
//   return response
// }, function (error) {
//   // 超出 2xx 范围的状态码都会触发该函数。
//   // 对响应错误做点什么
//   if (error.response.status === 500) {
//     // 这里可以写处理逻辑
//     console.error('请求发生了500错误，响应数据为：', error.response.data)
//   }

//   return Promise.reject(error)
// })
axios.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    console.error('请求失败，错误信息为：', error)
    return Promise.reject(error)
  }
)
export default service
