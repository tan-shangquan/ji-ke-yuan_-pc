import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login.vue'
import Layout from '@/layout'
import store from '@/store'
import LookData from '@/views/LookData.vue'
Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/layout',
    component: Layout,
    redirect: '/layout/lookdata',
    children: [
      { path: 'lookdata', component: LookData },
      { path: 'CMS', component: () => import('@/views/CMS.vue') },
      { path: 'putout', component: () => import('@/views/Putout.vue') }
    ]
  }

]

const router = new VueRouter({
  routes
})
// 白名单
const whiteList = ['/login', '/404']
// 路由前置守卫
router.beforeEach((to, from, next) => {
  const token = store.state.user.token
  if (token) {
    next()
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next({ path: '/login', replace: true })
    }
  }
})
export default router
